package com.galvanize.formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter{


    @Override
    public String format(Booking input)
    {
        String output;
        if(input.getRoomType().equals(Booking.Type.Conference))
        {
            output = "type,room number,start time,end time\n" + input.getRoomType() + " Room," + input.getRoomNumber() + "," + input.getStartTime() + ","
                    + input.getEndTime();
        }
        else
        {
            output = "type,room number,start time,end time\n" + input.getRoomType() + "," + input.getRoomNumber() + "," + input.getStartTime() + ","
                    + input.getEndTime();
        }
        return output;
    }
}
