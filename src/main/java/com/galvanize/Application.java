package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;

import java.awt.print.Book;

public class Application {

    public static Formatter getFormatter(String firstInput)
    {
        if(firstInput.equalsIgnoreCase("html"))
        {
            Formatter formatted = new HTMLFormatter();
            return formatted;
        } else if (firstInput.equalsIgnoreCase("json"))
        {
            Formatter formatted = new JSONFormatter();
            return formatted;
        } else  {
            Formatter formatted = new CSVFormatter();
            return formatted;
        }
    }
    public static void main(String[] args) {
        Booking qqq = new Booking(Booking.parse(args[0]));
        Formatter test2 = getFormatter(args[1]);
        System.out.println(test2.format(qqq));


    }
}