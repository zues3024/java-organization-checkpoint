package com.galvanize;

public class Booking {


    public enum Type{

        Conference, Suite, Auditorium, Classroom
    }
    private static Type type;
    private static String roomNumber = "";
    private static String startTime;
    private static String endTime;

    public Booking(Booking firstInput) {
        this(firstInput.getRoomType(), firstInput.getRoomNumber(), firstInput.getStartTime(), firstInput.getEndTime());

    }

    public Booking(Type room, String roomNumber, String startTime, String endTime)
    {
        this.type = room;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }
    public Booking()
    {
        type = null;
        roomNumber = null;
        startTime = null;
        endTime = null;
    }


    public static Booking parse(String bookingCode)
    {
        String[] parser = bookingCode.toLowerCase().split("-",3);
        String rmNumber = "";
        for(int i= 1; i< parser[0].length(); i++)
        {
            rmNumber += parser[0].charAt(i);
        }

        Booking res = new Booking(Type.Auditorium, rmNumber, parser[1], parser[2]);
        if (parser[0].startsWith("s")) {
            type = Type.Suite;
        }else if (parser[0].startsWith("r")) {
            type = Type.Conference;
        }else
            type = Type.Classroom;

        return res;
    }

    public String getRoomNumber() {
        return roomNumber;
    }
    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public Type getRoomType() {
        return type;
    }


}
